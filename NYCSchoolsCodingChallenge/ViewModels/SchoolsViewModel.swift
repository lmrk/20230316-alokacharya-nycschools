//
//  SchoolsViewModel.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import Foundation
import Combine

class SchoolsViewModel {
    private let networkService: NetworkService

    var listOfSchools = CurrentValueSubject<[School], Never>([School]())
    var isPaginating = false
    var error = CurrentValueSubject<Error?, Never>(nil)
    
    private let limit = 30 // Total limit per call
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
}

@MainActor
extension SchoolsViewModel {
    func fetchSchools(isPaginating: Bool = false) {
        Task {
            do {
                if isPaginating {
                    self.isPaginating = true
                }
                let listOfSchools: [School] = try await networkService.fetch(url: defaultSchoolURL)
                self.listOfSchools.value.append(contentsOf: listOfSchools)
                if isPaginating {
                    self.isPaginating = false
                }
                self.error.send(nil)
            } catch {
                self.error.send(error)
            }
        }
    }
    
    func fetchSATScoreForSchool(with id: String) async -> SATScroreModel? {
        do {
            let satData: [SATScroreModel] = try await networkService.fetch(url: SATDataURL(dbn: id))
            self.error.send(nil)
            return satData.first
        } catch {
            print("DEBUG **** \(error)")
            self.error.send(error)
        }
        return nil
    }
    
    func findSchool(with id: String) -> EnumeratedSequence<[School]>.Element? {
        guard let obj = self.listOfSchools.value.enumerated().first(where: { $0.element.dbn == id }) else { return nil}
        return obj
    }
}

extension SchoolsViewModel {
    // How much to offset by, will change depending on current total
    private var offset: Int { listOfSchools.value.count }
}

extension SchoolsViewModel {
    private var defaultSchoolURL: String {
        "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=\(limit)&$offset=\(offset)"
    }
    
    private func SATDataURL(dbn: String) -> String {
        "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
    }
}
