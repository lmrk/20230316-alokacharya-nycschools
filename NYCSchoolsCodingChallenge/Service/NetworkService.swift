//
//  NetworkService.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case invalidConnection
    case invalidDecode
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case .invalidConnection:
            return "Connection is Invalid, please try again"
        case .invalidDecode:
            return "Unable to parse data, please try again"
        }
    }
}

class NetworkService {
    static let shared = NetworkService()
    
    func fetch<T: Codable>(url: String) async throws -> T {
        guard let url = URL(string: url) else {
            throw NetworkError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        if let httpResponse = response as? HTTPURLResponse {
            if httpResponse.statusCode != 200 {
                throw NetworkError.invalidConnection
            }
        }
        
        print("DEBUG **** \(String(describing: data.prettyPrintedJSONString))")
    
        do {
            let result = try JSONDecoder().decode(T.self, from: data)
            return result
        } catch {
            throw NetworkError.invalidDecode
        }
    }
}
