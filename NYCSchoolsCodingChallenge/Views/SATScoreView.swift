//
//  SATScoreView.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import SwiftUI

struct SATScoreView: View {
    var satScoreModel: SATScroreModel?
    
    private let maxScore: Double = 800 // Max score per section
    
    var body: some View {
        if let satScoreModel {
            fullContentView(satScoreModel: satScoreModel)
        } else {
            noSATDataView
        }
    }
}

extension SATScoreView {
    var noSATDataView: some View {
        VStack (alignment: .center){
            Text("There is no SAT Data for this school")
                .font(.title3)
                .foregroundColor(.gray)
        }
    }
    
    func fullContentView(satScoreModel: SATScroreModel) -> some View {
        VStack(alignment: .leading, spacing: 16){
            Text("SAT Score per Subject (Average)")
                .font(.subheadline)
                .fontWeight(.bold)
                .padding(.bottom, 5)
            
            HStack(spacing: 48){
                circleDataWithText(title: "Math",
                                   averageScore: satScoreModel.mathAvgScore)
                
                circleDataWithText(title: "Reading",
                                   averageScore: satScoreModel.criticalReadingAvgScore)
                
                circleDataWithText(title: "Writing",
                                   averageScore: satScoreModel.writingAvgScore)
            }
        }
        .padding()
    }
    
    func circleDataWithText(title: String, averageScore: String) -> some View {
        VStack(alignment: .center, spacing: 16){
            CircularProgressView(title: averageScore,
                                 progress: toProgressValue(score: averageScore) / maxScore)
            .frame(width: 75, height: 75)
            
            Text(title)
                .font(.subheadline)
        }
    }
}

extension SATScoreView {
    func toProgressValue(score: String) -> Double {
        guard let scoreAsDouble = Double(score) else { return 0 }
        return scoreAsDouble
    }
}


struct CircularProgressView: View {
    var title: String
    var progress: Double
    @State var progressBar: Double = 0
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(
                    Color.pink.opacity(0.5),
                    lineWidth: 10
                )
            Text(title)
                .font(.title3)
                .fontWeight(.semibold)
            Circle()
                .trim(from: 0, to: progressBar)
                .stroke(
                    Color.pink,
                    style: StrokeStyle(
                        lineWidth: 10,
                        lineCap: .round
                    )
                )
                .rotationEffect(.degrees(-90))
                .animation(.easeInOut(duration: 2.5), value: progressBar)
        }
        .onAppear {
            withAnimation {
                self.progressBar = progress
            }
        }
    }
}
