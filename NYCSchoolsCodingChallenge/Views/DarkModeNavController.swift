//
//  DarkModeNavController.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import UIKit

// I didn't want to spend hours trying to figure out a bug regarding NavController so I just created this DarkModeAwareNavController

class DarkModeAwareNavigationController: UINavigationController {
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        self.updateBarTintColor()
        self.updateNavBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.updateBarTintColor()
        self.updateNavBar()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.updateBarTintColor()
        self.updateNavBar()
    }
    
    private func updateBarTintColor() {
        self.navigationBar.barTintColor = interFaceStyle == .dark ? .black : .white
    }
    
    private func updateNavBar() {
        let attribute: [NSAttributedString.Key : Any] = [
            .foregroundColor : interFaceStyle == .dark ? UIColor.white : UIColor.black
        ]
        self.navigationBar.titleTextAttributes = attribute
        self.navigationBar.largeTitleTextAttributes = attribute
        
        self.navigationBar.backgroundColor = interFaceStyle == .dark ? .black : .white
        self.view.backgroundColor = interFaceStyle == .dark ? .black : .white
        self.navigationBar.isTranslucent = false
    }
    
    private var interFaceStyle: UIUserInterfaceStyle { UITraitCollection.current.userInterfaceStyle }
}
