//
//  SchoolDetailViewController.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import Foundation
import UIKit
import SwiftUI

// Too small of a class for it to have a viewmodel
class SchoolDetailViewController: UIViewController {
    let schoolData: School
    let satData: SATScroreModel?
    
    lazy var schoolDetailViewController = UIHostingController(rootView: SchoolDetailView(schoolData: schoolData))
    
    init(schoolData: School,
         satData: SATScroreModel?) {
        self.schoolData = schoolData
        self.satData = satData
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SchoolDetailViewController {
    private func setupView() {
        self.title = schoolData.schoolName
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        setupSchoolDetailView()
        setupSATView()
    }
    
    private func setupSchoolDetailView() {
        schoolDetailViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        addChild(schoolDetailViewController)
        view.addSubview(schoolDetailViewController.view)
        NSLayoutConstraint.activate([
            schoolDetailViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            schoolDetailViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            schoolDetailViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func setupSATView() {
        let satView = SATScoreView(satScoreModel: satData)
        let contentView = UIHostingController(rootView: satView)
        contentView.view.translatesAutoresizingMaskIntoConstraints = false
        
        addChild(contentView)
        view.addSubview(contentView.view)
        NSLayoutConstraint.activate([
            contentView.view.topAnchor.constraint(equalTo: schoolDetailViewController.view.bottomAnchor),
            contentView.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentView.view.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}


