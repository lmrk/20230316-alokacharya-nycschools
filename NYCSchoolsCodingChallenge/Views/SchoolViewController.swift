//
//  SchoolViewController.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import UIKit
import Combine

class SchoolViewController: UIViewController {
    let viewModel: SchoolsViewModel
    
    private let tableView: UITableView = {
        let tblView = UITableView(frame: .zero, style: .grouped)
        tblView.translatesAutoresizingMaskIntoConstraints = false
        return tblView
    }()
    
    private let loadingView: UIActivityIndicatorView = {
        let avView = UIActivityIndicatorView(style: .large)
        avView.translatesAutoresizingMaskIntoConstraints = false
        return avView
    }()
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(networkService: NetworkService) {
        self.viewModel = SchoolsViewModel(networkService: networkService)
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupViews()
        setupSubscriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupSubscriptions() {
        //Subscribes to the array of list of schools
        viewModel.listOfSchools
            .sink { [weak self](_) in
                guard let self else { return }
                DispatchQueue.main.async {
                    self.loadingView.stopAnimating()
                    self.tableView.tableFooterView = nil
                    self.tableView.reloadData()
                }
            }
            .store(in: &subscriptions)
        
        // Subscribes to the error to show the error alert
        viewModel.error
            .sink { [weak self] error in
                guard let self, let error else { return }
                self.showAlert(message: error.localizedDescription)
            }
            .store(in: &subscriptions)
        
        viewModel.fetchSchools()
    }
    
    private func setupViews() {
        self.title = "NYC Schools"
        setupTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SchoolViewController {
    
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        setupLoadingView()
    }
    
    private func setupLoadingView() {
        tableView.addSubview(loadingView)
        
        NSLayoutConstraint.activate([
            loadingView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor)
        ])
        
        loadingView.startAnimating()
    }
    
    private func showAlert(message: String) {
        let alertController = UIAlertController(title: "Error",
                                                message: message,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        
        alertController.addAction(okAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
}

extension SchoolViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listOfSchools.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let item = viewModel.listOfSchools.value[indexPath.row]
        cell.textLabel?.text = item.schoolName
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.listOfSchools.value[indexPath.row]
        Task {
            self.loadingView.startAnimating()
            let satData = await viewModel.fetchSATScoreForSchool(with: school.dbn)
            tableView.deselectRow(at: indexPath, animated: true)
            self.loadingView.stopAnimating()
            let vc = SchoolDetailViewController(schoolData: school, satData: satData)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset
        
        if distanceFromBottom < height {
            guard !viewModel.isPaginating else { return }
            self.tableView.tableFooterView = createFooterSpinner()
            viewModel.fetchSchools(isPaginating: true)
        }
    }
    
    private func createFooterSpinner() -> UIView {
        let footerView = UIView(frame: .init(x: 0, y: 0, width: view.frame.size.width, height: 100))
        
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
}

