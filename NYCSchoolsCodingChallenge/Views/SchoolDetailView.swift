//
//  SchoolDetailView.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import SwiftUI
import MapKit

// Using SwiftUI to implement data presentation instead of using UIKit which takes much longer
struct SchoolDetailView: View {
    var schoolData: School
    @Environment(\.openURL) var openURL
    
    var body: some View {
        VStack (alignment: .leading, spacing: 16){
            if let schoolName = schoolData.schoolName {
                customText(string: "School Name: \(schoolName)")
            }
            
            if let schoolPhone = schoolData.phoneNumber {
                Button {
                    let phone = "tel://"
                    let phoneNumberFormatted = phone + schoolPhone
                    guard let url = URL(string: phoneNumberFormatted) else { return }
                    UIApplication.shared.open(url)
                } label: {
                    customText(string: "Phone Number: \(schoolPhone)")
                }
            }
            
            if let schoolEmail = schoolData.schoolEmail {
                Button {
                    // Here we would add the feature for allowing the user to be sent to the email view
                } label: {
                    customText(string: "Email: \(schoolEmail)")
                }
            }
            
            if let schoolWebsite = schoolData.website {
                Button {
                    openURL(URL(string: "https://\(schoolWebsite)")!)
                } label: {
                    customText(string: "Website: \(schoolWebsite)")
                }
            }
            
            if let totalStudents = schoolData.totalStudents {
                customText(string: "Total Students: \(totalStudents)")
            }
            
            
            if let formattedAddress = formattedAddress {
                customText(string: "Address: \(formattedAddress)")
            }
            
            if let coordinates = coordinates {
                Map(coordinateRegion: .constant(.init(center: coordinates,
                                                      span: .init(latitudeDelta: 0.01,
                                                                  longitudeDelta: 0.01))))
                    .frame(height: 175)
                    .cornerRadius(5)
            }
           
        }
        .padding(8)
    }
}

extension SchoolDetailView {
    func customText(string data: String) -> some View {
        Text(data)
            .font(.subheadline)
            .minimumScaleFactor(0.7)
            .fontWeight(.regular)
    }
}

extension SchoolDetailView {
    var formattedAddress: String? {
        "\(schoolData.primaryAddress ?? ""), \(schoolData.city ?? ""), \(schoolData.state ?? "") \(schoolData.zip ?? "")"
    }
    
    var coordinates: CLLocationCoordinate2D? {
        guard let longitude = Double(schoolData.longitude ?? ""), let latitude = Double(schoolData.latitude ?? "") else { return nil }
        return .init(latitude: latitude, longitude: longitude)
    }
}
