//
//  SATScoreModel.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import Foundation

// Model for the SAT score data for individual school
struct SATScroreModel: Codable {
    let dbn: String
    let schoolName: String
    let numOfTestTakers: String
    let criticalReadingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}
