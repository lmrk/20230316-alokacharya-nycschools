//
//  SchoolModel.swift
//  NYCSchoolsCodingChallenge
//
//  Created by Alok Acharya on 3/15/23.
//

import Foundation

// Individual Schools
struct School: Codable {
    let dbn: String
    let totalStudents: String
    let schoolName: String?
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String?
    let primaryAddress: String?
    let city: String?
    let zip: String?
    let state: String?
    let latitude: String?
    let longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case totalStudents = "total_students"
        case schoolName = "school_name"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case primaryAddress = "primary_address_line_1"
        case city, zip
        case state = "state_code"
        case latitude, longitude
    }
}
